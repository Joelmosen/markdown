# Markdown Cheat Sheet

Det här dokumentet är skrivet i ett format som kallas *Markdown*. Markdown är ett enkelt sätt att formattera textdokument.

## Rubriker

För att göra en rubrik skriver man en hash `#` i början av raden, som i början av rubriken i detta avsnitt.

Ju fler `#` i början av raden, desto mindre rubrik.

Skriv en `#` i början av raden där det står *Rubrik 1*, `##` på raden
*Rubrik 3* och `###` på raden *Rubrik 3*.

# Rubrik 1

## Rubrik 2 

### Rubrik 3

## Betoning

För att få viss ord att sticka ut kan man betona det genom att skriva en
asterisk `*` framför och efter ordet.

Betona ordet *emphasis* i denna mening genom att omge det med `*`.

Betona orden **strong emphasis** i denna mening genom att omge dem båda med `**`.

## Listor

Det finns två typer av listor:

* Punktlistor
* Numrerade listor

Punktlistor börjar med en asterisk på varje rad. Man kan göra en fruktsallad av
ingredienserna nedan. Gör om dem till en punktlista.

* Banan
* Vindruvor
* Kiwi
* Passionsfrukt
* Mango

Numrerade listor börjar med nummer och punkt. Listan nedan är inte komplett.
Lägg till punkterna 3 - 5 så att det blir rätt.

1. Skala bananen och skär den i skivor.
2. Skölj vindruvorna och dela dem.
3. Skala kiwi och skär i tärningar.
4. Gröp ur passionsfrukten.
5. Skala mangon och skär i stavar.
6. Blanda alla frukter i en skål.

## Kod

Genom att börja en rad med fyra blanksteg eller en tab gör man att texten blir
ett kodblock.

Skriv 4 blanksteg i början av nedanstående rader för att göra ett kodblock.

    x = 3
    y = 5
    if x > y:
    print("x is bigger than y")
    else:
    print("y is bigger than x")

Kodavsnitt kan även göras genom att ange text inom ` tecken.

Denna rad innehåller lite `kod som renderas annorlunda`. Gör ett kodavsnitt av
resten av raden: `x = foo() + bar`

## Länkar

Du kan göra en länk till en annan sida genom att ange texten som ska vara en
länk inom hakparanteser, [ och ]. Efter texten inom hakparanteser ska det finnas
en address till sidan du ska länka till inom vanliga paranteser ( och ).

`En länk till [Markdown Syntax](http://daringfireball.net/projects/markdown/syntax)`

blir

En länk till [Markdown Syntax](http://daringfireball.net/projects/markdown/syntax)

Gör ordet [Wikipedia](www.wikipedia.org) i denna mening till en länk till Wikipedias hemsida.

## Bilder

En länk till en bild gör du så här:

`![Patwic](http://patwic.com/wp-content/uploads/2013/03/elephants_pyramid_1_transparent_web1.png)`

![Patwic](http://patwic.com/wp-content/uploads/2013/03/elephants_pyramid_1_transparent_web1.png)

Texten inom hakparanteserna är en bildtext och bildens address anges inom
paranteser.

![Elefant](http://www.google.se/search?hl=sv&site=imghp&tbm=isch&source=hp&biw=792&bih=453&q=elefant&oq=elefant&gs_l=img.3..0l10.1677.3734.0.4393.7.4.0.3.3.0.58.218.4.4.0...0.0...1ac.1.8.img.TOZCMYBKI5E#hl=sv&site=imghp&tbm=isch&sa=1&q=tomat&oq=tomat&gs_l=img.3..0l10.3240.5457.0.5726.11.9.0.0.0.2.65.436.8.8.0...0.0...1c.1.8.img.a20-XWrlBs4&bav=on.2,or.r_qf.&bvm=bv.44770516,d.bGE&fp=2118eb27e1e0ec8c&biw=792&bih=453&imgrc=INN9o_zEiKkoHM%3A%3BpJb1Rfl_dGLdzM%3Bhttp%253A%252F%252Fwww.freewebs.com%252Fwavebird%252Ftomat_1172844651.jpg%3Bhttp%253A%252F%252Fwww.freewebs.com%252Fwavebird%252F2on2klaner.htm%3B250%3B253)
